[![Version](https://img.shields.io/badge/dynamic/json?color=blue&label=Version&prefix=v&query=version&url=https%3A%2F%2Fgitlab.com%2Ffoundry-vtt%2Ffoundry-vtt-share-ldj%2F-%2Fraw%2Fmaster%2Fmodule.json)](https://gitlab.com/foundry-vtt/foundry-vtt-share-ldj) [![Min Core](https://img.shields.io/badge/dynamic/json?color=brightgreen&label=Min%20Core&prefix=v&query=minimumCoreVersion&url=https%3A%2F%2Fgitlab.com%2Ffoundry-vtt%2Ffoundry-vtt-share-ldj%2F-%2Fraw%2Fmaster%2Fmodule.json)](http://foundryvtt.com/) [![Compatible](https://img.shields.io/badge/dynamic/json?color=brightgreen&label=Compatible&prefix=v&query=compatibleCoreVersion&url=https%3A%2F%2Fgitlab.com%2Ffoundry-vtt%2Ffoundry-vtt-share-ldj%2F-%2Fraw%2Fmaster%2Fmodule.json)](http://foundryvtt.com/) [![DnD 5e Compatible](https://img.shields.io/badge/DnD_5e_Compatible-1.24-brightgreen)](https://foundryvtt.com/packages/dnd5e/)

# Foundry VTT Share - Livro do Jogador

Um módulo contendo raças, classes, antecedentes, habilidades, magias e outras coisas presentes no Livro do Jogador de D&D 5e em português.

OBS. Distâncias estão em metros e/ou quilômetros e pesos estão em quilogramas.

## Instalação
Na tela de SETUP do FoundryVTT, escolha a opção "Add-On Modules" e então clique em "Install Module" e coloque a seguinte URL no campo "Manifest URL":

```https://gitlab.com/foundry-vtt/foundry-vtt-share-ldj/raw/master/module.json```
  
Alternativamente, você pode fazer o download do seguinte arquivo [dnd5e-phb-ptbr.zip](https://gitlab.com/foundry-vtt/foundry-vtt-share-ldj/-/jobs/artifacts/master/raw/dnd5e-phb-ptbr.zip?job=build) e extraí-lo na pasta ```Data/modules```.
  
Após ter feito isso, você deve ativar o módulo nas configurações do mundo em que pretende usá-lo e então buscar explorar a aba de compêndios.
